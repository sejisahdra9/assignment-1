// draw.cpp

//
// IMPORTANT!
//
// Don't change this file in any way! Keep it exactly as it is. All your code
// goes in draw.h.
//

#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
#include "draw.h"

using namespace std;

int main()
{
    sf::RenderWindow win(sf::VideoMode(WIDTH, HEIGHT), "Assignment 1");
    win.setFramerateLimit(60);
    bool drawn = false;

    // main animation loop
    while (win.isOpen()) {
        // check all window events that were triggered; always check for
        // sf::Event::Closed so that the window can be closed
        sf::Event event;
        while (win.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                win.close();
            }
        }

        if (!drawn) {
            win.clear(sf::Color::Black);
            process_lines(win);
            drawn = true;
        }

        // end the current frame and display the window
        win.display();
    } // while
} // main
